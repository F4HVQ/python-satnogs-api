from __future__ import print_function

import json
import re

import requests
from datetime import datetime


NETWORK_DEV_BASE_URL = 'https://network-dev.satnogs.org'
NETWORK_BASE_URL     = 'https://network.satnogs.org'
DB_BASE_URL          = 'https://db.satnogs.org'
DB_DEV_BASE_URL      = 'https://db-dev.satnogs.org'


def is_satnogs_server_available(prod=True):
    # type: (bool) -> bool
    """ Check if server is reachable

    :param: prod: Use production URL
    :return: Boolean, True if server available
    """

    url = NETWORK_BASE_URL if prod else NETWORK_DEV_BASE_URL

    try:
        r = requests.get(url)
        r.raise_for_status()
    except requests.exceptions.HTTPError:
        return False
    else:
        return True


def get_paginated_endpoint(url, max_entries=None):
    r = requests.get(url=url)
    r.raise_for_status()

    data = r.json()

    while 'next' in r.links  and (not max_entries or len(data) < max_entries):
        next_page_url = r.links['next']['url']

        r = requests.get(url=next_page_url)
        r.raise_for_status()

        data.extend(r.json())

    return data


def generate_satnogs_url(baseurl, dict_key_value):
    # type (str, dict) -> str
    """ TODO

    :param baseurl: str:
    :param dict_key_value: Dictionary containing keys/values
    :return: str : The url
    """

    filter = "&".join("{}={}".format(k, v) for k, v in dict_key_value.items())
    url = "{}{}".format(baseurl, filter)

    return url


def get_data(url):
    # type: (str) -> TODO
    """ TODO

    :param url: TODO
    :return: TODO
    """

    r = requests.get(url=url)
    r.raise_for_status()

    return r.json()


def fetch_observation_data_from_id(norad_ids, start, end, prod=True):
    # type (list, datetime, datetime) -> list
    """ Get all observations of the satellite with the given `norad_id` in the given timeframe
    https://network.satnogs.org/api/observations/?satellite__norad_cat_id=25544&start=2018-06-10T00:00&end=2018-06-15T00:00

    :param norad_ids: TODO
    :param start: TODO
    :param end: TODO
    :param prod: Use production URL
    :return: TODO
    """

    observations = list()

    for norad_id in norad_ids:
        query_parameters = {'format': "json",
                            'satellite__norad_cat_id': norad_id,
                            'start': start.isoformat(),
                            'end': end.isoformat()
                            }

        observation_url = generate_satnogs_url("{}/api/observations/?".format(NETWORK_BASE_URL if prod else NETWORK_DEV_BASE_URL),
                                               query_parameters)
        observations += get_paginated_endpoint(observation_url)

    return observations


def fetch_observation_data_from_ground_station_id(ground_station_id, start, end, prod=True):
    # type (list, datetime, datetime) -> list
    """ Get all observations of the satellite with the given `norad_id` in the given timeframe
    https://network.satnogs.org/api/observations/?satellite__norad_cat_id=25544&start=2018-06-10T00:00&end=2018-06-15T00:00

    :param ground_station_id: TODO
    :param start: TODO
    :param end: TODO
    :param prod: Use production URL
    :return: TODO
    """

    query_parameters = {'format': "json",
                        'ground_station_id': ground_station_id,
                        'start': start.isoformat(),
                        'end': end.isoformat()
                        }

    observation_url = generate_satnogs_url("{}/api/observations/?".format(NETWORK_BASE_URL if prod else NETWORK_DEV_BASE_URL),
                                           query_parameters)
    observations = get_paginated_endpoint(observation_url)

    # Current prod is broken and can't filter on NORAD ID correctly,
    # use client-side filtering instead
    observations = list(observation for observation in observations
                        if observation['ground_station'] == ground_station_id)

    return observations


def fetch_observation_data(observation_ids, prod=True):
    # type(list, bool) -> list
    """ Fetch observation data from a specific observation id list

    :param observation_ids: list : The observation id list to fetch
    :param prod: bool: Use production URL
    :return list
    """

    observations = list()
    for observation_id in observation_ids:
        if str(observation_id) == "None":
            continue

        query_parameters = {'format': "json",
                            'id': observation_id
                            }

        observation_url = generate_satnogs_url("{}/api/observations/?".format(NETWORK_BASE_URL if prod else NETWORK_DEV_BASE_URL),
                                               query_parameters)
        observations += get_paginated_endpoint(observation_url)

    return observations


def fetch_ground_station_data(ground_station_ids, prod=True):
    # type(list, bool) -> list
    """ Fetch ground station metadata from network

    :param ground_station_ids: The list of ground station id
    :param prod: Use production URL
    :return: list
    """

    ground_stations = []
    for ground_station_id in ground_station_ids:
        # Skip frames from deleted groundstations, indidcated as ID 'None'
        if str(ground_station_id) == 'None':
            print("Skipping ground station 'None'")
            continue

        query_parameters = {'format': "json",
                            'id': "{}".format(ground_station_id)
                            }

        ground_station_url = generate_satnogs_url("{}/api/stations/?".format(NETWORK_BASE_URL if prod else NETWORK_DEV_BASE_URL), query_parameters)
        ground_stations.append(get_data(ground_station_url))

    return ground_stations


def fetch_satellite_data(norad_cat_id):
    # type (int) -> list
    """ Fetch satellite metadata from network

    :param norad_cat_id: The satellite NORAD id
    :return: TODO
    """

    query_parameters = {'format': "json",
                        'norad_cat_id': "{}".format(norad_cat_id)
                        }

    satellite_url = generate_satnogs_url("{}/api/satellites/?".format(DB_BASE_URL),
                                         query_parameters)
    return get_data(satellite_url)


def fetch_tle_of_observation(observation_id, prod=True):
    # type (int) -> list
    """ Fetch TLE data for a specific observation_id

    :param observation_id: TODO
    :param prod: Use production URL
    :return: TODO
    """

    observation_url = "{}/observations/{}".format((NETWORK_BASE_URL if prod else NETWORK_DEV_BASE_URL),
                                                  observation_id)
    r = requests.get(observation_url)
    observation_page_html = r.text

    regex = r"<pre>1 (.*)<br>2 (.*)</pre>"
    matches = re.search(regex, observation_page_html)

    obs_tle_2 = '1 ' + matches.group(1)
    obs_tle_3 = '2 ' + matches.group(2)

    return [obs_tle_2, obs_tle_3]


def get_tle_of_observation(tle):
    # type (str) -> list
    """ Fetch TLE data from string

    :param tle
    :return: TODO
    """

    tle_data = json.loads(tle)
    line1 = tle_data["tle1"]
    line2 = tle_data["tle2"]
    return [line1, line2]


def fetch_telemetry(norad_id, url):
    # http://db-dev.satnogs.org/api/telemetry/?satellite=43595

    query_str = '{}/api/telemetry/?satellite={}'

    url = query_str.format(url, norad_id)

    telemetry = get_paginated_endpoint(url)

    return telemetry


def fetch_transmitters(norad_id, url):
    # http://db-dev.satnogs.org/api/transmitters/?satellite__norad_cat_id=25544

    query_str = '{}/api/transmitters/?satellite__norad_cat_id={}'

    url = query_str.format(url, norad_id)

    transmitters = get_paginated_endpoint(url)
    return transmitters


def post_telemetry(norad_id,
                   source,  # Receiver Callsign
                   lon,
                   lat,
                   timestamp,
                   frame,
                   base_url=DB_DEV_BASE_URL):
    payload = {'noradID': norad_id,
               'source': source,
               'timestamp': timestamp,
               'latitude': lat,
               'longitude': lon,
               'frame': frame}

    url = '{}/api/telemetry/'.format(base_url)
    r = requests.post(url, data=payload)

    if r.status_code != 201:
        print('ERROR {}: {}'.format(r.status_code, r.text))


def has_audio_url(observation_data):
    # type() -> bool
    """ Check if observation have an audio URL

    :param observation_data: The observation data to check
    :return bool:
    """
    if len(get_audio_url(observation_data)) > 0:
        ret = True
    else:
        ret = False
    return ret


def get_audio_url(observation_data):
    # type () -> str
    """ TODO

    :param observation_data:
    :return: TODO
    """
    audio_url = ""

    if observation_data is not None:
        if observation_data['archived']:
            audio_url = observation_data['archive_url']
        elif observation_data['payload'] is not None:
            audio_url = observation_data['payload']
        else:
            audio_url = "https://dummy_url/data_{}_{}.ogg".format(observation_data['id'], observation_data['start'].replace(':', '-'))
            audio_url = audio_url.replace('Z', '')

        return audio_url


def get_pass_timestamp(observation, moment):
    # type (TODO, int) -> None
    """ TODO

    :param observation: TODO
    :param moment: TODO
    :return: TODO
    """

    pass_time = {}

    if observation is not None:
        pass_start    = datetime.strptime(observation['start'], "%Y-%m-%dT%H:%M:%SZ")
        pass_end      = datetime.strptime(observation['end'], "%Y-%m-%dT%H:%M:%SZ")
        pass_duration = pass_end - pass_start
        pass_overhead = pass_start + (pass_duration / 2.0)

        pass_time[0] = pass_start
        pass_time[1] = pass_overhead
        pass_time[2] = pass_end

        return pass_time[moment]


#
#
# def fetch_observations_id_list_from_station_id(station_id, start_range, end_range):
#     # type: (int, datetime, datetime) -> list
#     """ Fetch observations list in date/time range for a specific station
#
#     :param station_id: TODO
#     :param start_range: TODO
#     :param end_range: TODO
#     :return: The list of observations
#     """
#
#     query_parameters = {'vetted_status': "good",
#                         'format': "json",
#                         'ground_station': station_id,
#                         'transmitter_mode': "APT",
#                         'start': "{}".format(start_range.isoformat()),
#                         'end': "{}".format(end_range.isoformat())
#                         }
#
#     observation_url     = generate_satnogs_url("{}/api/observations/?".format(NETWORK_BASE_URL), query_parameters)
#     observations        = get_paginated_endpoint(observation_url)
#     observations_id_list = []
#     for obs in observations:
#         observations_id_list.append(obs['id'])
#
#     return observations_id_list
#
#
# def fetch_observations_id_list_from_satellite_id(norad_cat_id, start_range, end_range):
#     # type: (int, datetime, datetime) -> list
#     """ TODO
#
#     :param norad_cat_id: TODO
#     :param start_range: TODO
#     :param end_range: TODO
#     :return: The list of observations
#     """
#
#     query_parameters = {'satellite__norad_cat_id': "{}".format(norad_cat_id),
#                         'format': "json",
#                         'start': "{}".format(start_range.isoformat()),
#                         'end': "{}".format(end_range.isoformat())
#                         }
#
#     observation_url = generate_satnogs_url("{}/api/observations/?".format(NETWORK_BASE_URL), query_parameters)
#     observations = get_paginated_endpoint(observation_url)
#     observations_id_list = []
#     for obs in observations:
#         observations_id_list.append(obs['id'])
#
#     return observations_id_list
